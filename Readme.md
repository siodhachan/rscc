# To the reviewers

## Prerequisites:

* have docker installed and running
* have a terminal ready( via wsl or native does not matter, `make` should be available )
* make sure port 8000 is free (sry, no argument for that available)

## How to evaluate:

To "run" the application please follow these steps:

1. Run `make serve` in the projects root directory
2. Run `make launch` or open the
   url [http://127.0.0.1:8000/equipment/stock-projection/1](http://127.0.0.1:8000/equipment/stock-projection/1)

At that point no data will be returned though since the database is still empty. To simulate incoming "bookings" you can
run `make simulate` and enter any data-points as required ( which you can then view by calling the url above).

To run the tests, enter `make test`. This will also include cs-fixer and phpstan - just so you don't have to run them
yourself;)

## Why the app is the way it is

The description for the challenge provides a decent amount of information surrounding the problem domain; but imho the
actual required solution has to cover a very narrow subset of the narrative ( I might be very wrong about that of
course;)):

In my understanding it is required to provide an API that delivers information on the stock of equipments over a certain
period (and that per day). For that, we need: "when, what, how many and where" - any other information is not required
and the "what"
and "where" can be sufficiently represented by id's only. Hence, there are no complex entities / relations nor core /
domain objects in this solution; all it does (needs to do) is a) provide means to track changes (in/out) of equipments
by location and date b)
provide an API to retrieve the information for a given (or default) timeframe.

This solution aims to provide this (minimal) required subset - it tracks changes sequentially in one data source/table
and retrieves it via one /controller/view. It assumes that any events with impact on equipment stocks come from external
systems and are handled in this context only. This is demonstrated by the BookingWasCreated`-Event which can be
simulated/triggered by the command mentioned above (so the CLI-Command would not exist in any other environment). In a
real-life example the event would then probably be a message.

In that context: That is why I did not provide any additional fixtures exceeding the minimal set needed to run the
tests. We do not need booking totals, location coordinates or similar. Actually, even using the ORM in this case is
already not really beneficial - but it made the developing process a little faster with all extra scripts/steps being
automatically available with doctrine fixtures / schema manipulations and using the maker bundle to create the (one)
entity. But: There still is one fixture and one entity, and I hope it demonstrates that I am aware of how and why to use
these concepts.

So In summary: This solution does what is asked - but at the very bare, lower, back in the corner, almost invisible end
of the requirements given. If more/a different solution was expected, then hopefully I can fill the gap with some
explaining and a good story.

Cheers, Sebastian

<sub>(Note: If you want to remove any images created, please
run `docker rmi $(docker images --format '{{.Repository}}:{{.Tag}}' | grep 'rscc-dev')`</sub>




