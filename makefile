container:=php

.PHONY: build
build:
	docker build ./ -f .docker/php/Dockerfile \
		--build-arg USER_ID=$(shell id -u) \
		--build-arg GROUP_ID=$(shell id -g) \
		--build-arg GIT_USER_EMAIL="$(shell git config user.email)" \
		--build-arg GIT_USER_NAME="$(shell git config user.name)" \
		-t rscc-dev:latest
#--
.PHONY: install
install:
	@docker run --rm --interactive --tty --name rscc-dev-install \
	--volume $(PWD):/project \
	--workdir="/project" \
	rscc-dev bash -c "composer install --no-scripts"
#--
.PHONY: attach
attach:
	@docker run --rm --interactive --tty --name rscc-dev \
	--volume $(PWD):/project \
	--workdir="/project" \
	rscc-dev bash
#--
.PHONY: serve
serve: build install
	@docker run --rm --interactive --tty --name rscc-dev-serve \
	--volume $(PWD):/project \
	--workdir="/project" \
	-p 8000:8000 \
	rscc-dev bash -c \
	"bin/console doctrine:schema:update --force \
		&& symfony serve"
#--
.PHONY: simulate
simulate:
	@docker run --rm --interactive --tty --name rscc-dev-simulate \
	--volume $(PWD):/project \
	--workdir="/project" \
	rscc-dev bash -c \
	"bin/console doctrine:schema:update --force \
		&& bin/console app:dispatch-equipment-booked"

#--
.PHONY: test
test: build install
	@docker run --rm --interactive --tty --name rscc-dev-test \
	--volume $(PWD):/project \
	--workdir="/project" \
	rscc-dev bash -c \
		"vendor/bin/php-cs-fixer fix --dry-run\
		&& vendor/bin/phpstan --level=3 analyse src tests \
	 	&& php bin/phpunit"

#--
.PHONY: launch
launch:
	@xdg-open "http://127.0.0.1:8000/equipment/stock-projection/1?dateFrom=2020-01-01&dateUntil=2020-02-01"