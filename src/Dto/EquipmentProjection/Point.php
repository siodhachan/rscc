<?php

namespace App\Dto\EquipmentProjection;

class Point
{
    public string $date;

    /**
     * @var array<int, Equipment>
     */
    public array $equipments = [];
}
