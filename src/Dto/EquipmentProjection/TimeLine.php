<?php

namespace App\Dto\EquipmentProjection;

class TimeLine
{
    /**
     * @var array<Point>
     */
    public array $points = [];
}
