<?php

namespace App\Dto;

use Webmozart\Assert\Assert;

class EquipmentProjectionQuery
{
    private int $locationId;

    private \DateTimeImmutable $dateFrom;

    private \DateTimeImmutable $dateUntil;

    public function __construct(int $locationId, \DateTimeImmutable $dateFrom, \DateTimeImmutable $dateUntil)
    {
        Assert::true($dateFrom < $dateUntil);
        Assert::greaterThan($locationId, 0);

        $this->locationId = $locationId;
        $this->dateFrom = $dateFrom;
        $this->dateUntil = $dateUntil;
    }

    public function getLocationId(): int
    {
        return $this->locationId;
    }

    public function getDateFrom(): \DateTimeImmutable
    {
        return $this->dateFrom;
    }

    public function getDateUntil(): \DateTimeImmutable
    {
        return $this->dateUntil;
    }
}
