<?php

namespace App\Controller;

use App\Dto\EquipmentProjectionQuery;
use App\Repository\EquipmentProjectionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

class EquipmentStockProjectionController extends AbstractController
{
    private EquipmentProjectionRepository $equipmentProjectionRepository;

    public function __construct(EquipmentProjectionRepository $equipmentProjectionRepository)
    {
        $this->equipmentProjectionRepository = $equipmentProjectionRepository;
    }

    /**
     * @throws \Exception
     */
    #[Route('/equipment/stock-projection/{locationId}', name: 'equipment_stock-projection')]
    public function index(int $locationId, Request $request): Response
    {
        $dateFrom = $this->getDateFromOrDefault($request);
        $dateUntil = $this->getDateUntilOrDefault($request);

        try {
            $this->assertDateRangesAreValid($dateFrom, $dateUntil);
        } catch (\InvalidArgumentException $e) {
            return $this->json('', 400);
        }

        $query = new EquipmentProjectionQuery(
            $locationId,
            $dateFrom,
            $dateUntil
        );

        return $this->json(
            $this->equipmentProjectionRepository->getTimeline($query)
        );
    }

    private function assertDateRangesAreValid(\DateTimeImmutable $dateFrom, \DateTimeImmutable $dateUntil): void
    {
        Assert::true($dateFrom <= $dateUntil);
        $diff = $dateFrom->diff($dateUntil);
        Assert::lessThanEq($diff->y, 3);
    }

    private function getDateFromOrDefault(Request $request): \DateTimeImmutable
    {
        try {
            return new \DateTimeImmutable(
                $request->get(
                    'dateFrom',
                    'now'
                )
            );
        } catch (\Throwable $e) {
            return new \DateTimeImmutable();
        }
    }

    private function getDateUntilOrDefault(Request $request): \DateTimeImmutable
    {
        $defaultDateUntil = (new \DateTimeImmutable('now'))->add(new \DateInterval('P30D'));
        try {
            return new \DateTimeImmutable(
                $request->get(
                    'dateUntil',
                    $defaultDateUntil->format(DATE_ATOM)
                )
            );
        } catch (\Throwable $e) {
            return $defaultDateUntil;
        }
    }
}
