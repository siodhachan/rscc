<?php

namespace App\DataFixtures;

use App\Entity\EquipmentChange;
use App\Entity\EquipmentChangeType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class EquipmentChangeFixtures extends Fixture
{
    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        $startingDate = new \DateTimeImmutable('2020-01-01');

        for ($locationId = 1; $locationId <= 2; ++$locationId) {
            for ($equipmentId = 1; $equipmentId <= 2; ++$equipmentId) {
                for ($daysOffset = 0; $daysOffset < 100; $daysOffset += 10) {
                    $equipmentChange = new EquipmentChange(
                        $equipmentId,
                        $locationId,
                        $startingDate->add(new \DateInterval('P'.$daysOffset.'D')),
                        0 === $daysOffset % 6 ? +2 : -2,
                        0 === $daysOffset % 6 ? EquipmentChangeType::Correction : EquipmentChangeType::BookingOut
                    );
                    $manager->persist($equipmentChange);
                }
            }
        }
        $manager->flush();
    }
}
