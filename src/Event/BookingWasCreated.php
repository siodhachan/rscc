<?php

namespace App\Event;

use Webmozart\Assert\Assert;

class BookingWasCreated
{
    private int $locationIdPickup;

    private int $locationIdReturn;

    private \DateTimeImmutable $dateFrom;

    private \DateTimeImmutable $dateUntil;

    /**
     * @var array<int, int>
     */
    private array $equipments;

    /**
     * @param int[] $equipments
     */
    public function __construct(int $locationIdPickup, int $locationIdReturn, \DateTimeImmutable $dateFrom, \DateTimeImmutable $dateUntil, array $equipments)
    {
        Assert::notEq($locationIdPickup, 0);
        Assert::notEq($locationIdReturn, 0);
        Assert::true($dateFrom <= $dateUntil);
        Assert::allInteger($equipments);
        Assert::allGreaterThan($equipments, 0);
        Assert::allInteger(array_keys($equipments));

        $this->locationIdPickup = $locationIdPickup;
        $this->locationIdReturn = $locationIdReturn;
        $this->dateFrom = $dateFrom;
        $this->dateUntil = $dateUntil;
        $this->equipments = $equipments;
    }

    public function getDateFrom(): \DateTimeImmutable
    {
        return $this->dateFrom;
    }

    public function getDateUntil(): \DateTimeImmutable
    {
        return $this->dateUntil;
    }

    /**
     * @return array<int, int>
     */
    public function getEquipments(): array
    {
        return $this->equipments;
    }

    public function getLocationIdPickup(): int
    {
        return $this->locationIdPickup;
    }

    public function getLocationIdReturn(): int
    {
        return $this->locationIdReturn;
    }
}
