<?php

namespace App\Entity;

use Webmozart\Assert\Assert;

enum EquipmentChangeType: string
{
    /*
     * These enums are really just for demonstrating that other cases could be handled.
     * For example a "stock up" could also be tracked and result in a stock change.
     */
    case StockUp = 'stock_up';
    case Correction = 'correction';
    case BookingOut = 'booking_out';
    case BookingReturn = 'booking_return';
    public static function assertValueIsValid(self $type, int $value): void
    {
        switch ($type) {
            case self::Correction:
                Assert::notEq($value, 0);
                break;
            case self::BookingOut:
                Assert::lessThan($value, 0);
                break;
            case self::BookingReturn:
            case self::StockUp:
                Assert::greaterThan($value, 0);
                break;
            default:
                throw new \LogicException('Type '.$type->name.' does not have a check against a valid value');
        }
    }
}
