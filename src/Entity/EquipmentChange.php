<?php

namespace App\Entity;

use App\Repository\EquipmentChangeORMRepository;
use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

#[ORM\Entity(repositoryClass: EquipmentChangeORMRepository::class)]
class EquipmentChange
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'integer')]
    private int $equipmentId;

    #[ORM\Column(type: 'integer')]
    private int $locationId;

    #[ORM\Column(type: 'date_immutable')]
    private \DateTimeImmutable $date;

    #[ORM\Column(type: 'integer')]
    private int $value;

    #[ORM\Column(type: 'string', length: 55)]
    private string $type;

    public function __construct(int $equipmentId, int $locationId, \DateTimeImmutable $date, int $value, EquipmentChangeType $type)
    {
        Assert::greaterThan($equipmentId, 0);
        Assert::greaterThan($locationId, 0);
        EquipmentChangeType::assertValueIsValid($type, $value);

        $this->equipmentId = $equipmentId;
        $this->locationId = $locationId;
        $this->date = $date;
        $this->value = $value;
        $this->type = $type->value;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}
