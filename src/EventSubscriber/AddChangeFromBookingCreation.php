<?php

namespace App\EventSubscriber;

use App\Entity\EquipmentChange;
use App\Entity\EquipmentChangeType;
use App\Event\BookingWasCreated;
use App\Repository\EquipmentChangeRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AddChangeFromBookingCreation implements EventSubscriberInterface
{
    private EquipmentChangeRepository $equipmentChangeRepository;

    public function __construct(EquipmentChangeRepository $equipmentChangeRepository)
    {
        $this->equipmentChangeRepository = $equipmentChangeRepository;
    }

    public static function getSubscribedEvents(): array
    {
        return [BookingWasCreated::class => 'onBookingWasCreated'];
    }

    public function onBookingWasCreated(BookingWasCreated $event): void
    {
        foreach ($event->getEquipments() as $equipmentId => $equipmentValue) {
            // Create one change for when the equipment leaves the location
            $entity = new EquipmentChange(
                $equipmentId,
                $event->getLocationIdPickup(),
                $event->getDateFrom(),
                $equipmentValue * -1,
                EquipmentChangeType::BookingOut
            );
            $this->equipmentChangeRepository->add($entity);

            // Create one change for when the equipment is to be returned
            $entity = new EquipmentChange(
                $equipmentId,
                $event->getLocationIdReturn(),
                $event->getDateUntil(),
                $equipmentValue,
                EquipmentChangeType::BookingReturn
            );
            $this->equipmentChangeRepository->add($entity);
        }
    }
}
