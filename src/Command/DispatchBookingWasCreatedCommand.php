<?php

namespace App\Command;

use App\Event\BookingWasCreated;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:dispatch-equipment-booked',
    description: 'Simulate "receiving" a booking created event (or message) for evaulation purposes only.',
)]
class DispatchBookingWasCreatedCommand extends Command
{
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('locationIdPickup', InputArgument::OPTIONAL)
            ->addArgument('locationIdReturn', InputArgument::OPTIONAL)
            ->addArgument('dateFrom', InputArgument::OPTIONAL)
            ->addArgument('dateUntil', InputArgument::OPTIONAL);
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');

        $givenLocationIdPickup = $input->getArgument('locationIdPickup');
        if (!$givenLocationIdPickup) {
            $givenLocationIdPickupQuestion = new Question('Id of the location to pickup from? (only positive integer) ');
            $givenLocationIdPickup = $helper->ask($input, $output, $givenLocationIdPickupQuestion);
        }

        $givenLocationIdReturn = $input->getArgument('locationIdReturn');
        if (!$givenLocationIdReturn) {
            $givenLocationIdReturnQuestion = new Question('Id of the location to return to? (only positive integer) ');
            $givenLocationIdReturn = $helper->ask($input, $output, $givenLocationIdReturnQuestion);
        }

        $givenDateFrom = $input->getArgument('dateFrom');

        if (!$givenDateFrom) {
            $givenDateFromQuestion = new Question('From what date? (format: YYYY-MM-DD)', date(DATE_ATOM));
            $givenDateFrom = $helper->ask($input, $output, $givenDateFromQuestion);
        }

        $givenDateUntil = $input->getArgument('dateUntil');

        if (!$givenDateUntil) {
            $givenDateUntilQuestion = new Question('Until what date? (format: YYYY-MM-DD) ', date(DATE_ATOM));
            $givenDateUntil = $helper->ask($input, $output, $givenDateUntilQuestion);
        }

        $equipments = [];
        $givenEquipmentIdQuestion = new Question('Id of the equipment? (only positive integer) (Enter 0 to stop adding equipment) ', 0);
        $givenEquipmentValueQuestion = new Question('How many of those? (only positive integer) ', 1);
        while ($givenEquipmentId = $helper->ask($input, $output, $givenEquipmentIdQuestion)) {
            $givenEquipmentValue = $helper->ask($input, $output, $givenEquipmentValueQuestion);
            $equipments[(int) $givenEquipmentId] = ($equipments[(int) $givenEquipmentId] ?? 0) + $givenEquipmentValue;
        }

        $event = new BookingWasCreated(
            (int) $givenLocationIdPickup,
            (int) $givenLocationIdReturn,
            new \DateTimeImmutable($givenDateFrom),
            new \DateTimeImmutable($givenDateUntil),
            $equipments
        );

        $this->eventDispatcher->dispatch($event);

        $io->success('The equipment change from your booking has been saved.');

        return Command::SUCCESS;
    }
}
