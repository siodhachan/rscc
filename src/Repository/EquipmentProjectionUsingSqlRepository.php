<?php

namespace App\Repository;

use App\Dto\EquipmentProjection\Equipment;
use App\Dto\EquipmentProjection\Point;
use App\Dto\EquipmentProjection\TimeLine;
use App\Dto\EquipmentProjectionQuery;
use Doctrine\DBAL\Connection;

class EquipmentProjectionUsingSqlRepository implements EquipmentProjectionRepository
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    public function getTimeline(EquipmentProjectionQuery $equipmentProjection): TimeLine
    {
        return $this->mapEntriesToView($this->fetchEntriesFromSource($equipmentProjection));
    }

    private function mapEntriesToView(array $entries): TimeLine
    {
        $timeLine = new TimeLine();
        foreach ($entries as $entry) {
            if (!isset($timeLine->points[$entry['day']])) {
                $timeLine->points[$entry['day']] = new Point();
            }
            $equipment = new Equipment();
            $equipment->equipmentId = $entry['ecjeid'];
            $equipment->aggregate = $entry['aggregate'] ?? 0;
            $timeLine->points[$entry['day']]->equipments[$equipment->equipmentId] = $equipment;
        }

        return $timeLine;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    private function fetchEntriesFromSource(EquipmentProjectionQuery $equipmentProjection): array
    {
        $query = <<<SQL
            WITH RECURSIVE days(day) AS (SELECT date(:dateFrom)
                                         UNION ALL
                                         SELECT DATE(day, '+1 days')
                                         FROM days
                                         WHERE day <= :dateUntil)
            
            SELECT *,
                   (SELECT SUM(ecs.value)
                    FROM equipment_change ecs
                    WHERE ecs.date <= days.day
                      AND ecs.location_id = :locationId
                      AND ecs.equipment_id = ecjeid
                    GROUP BY ecs.equipment_id, ecs.location_id) AS aggregate
            FROM days
                     CROSS JOIN (SELECT DISTINCT ecj.equipment_id ecjeid
                                 FROM equipment_change AS ecj
                                 WHERE ecj.location_id = :locationId AND ecj.date < :dateUntil)
            ORDER by days.day ASC
SQL;

        return $this->connection
            ->prepare($query)
            ->executeQuery([
                'locationId' => $equipmentProjection->getLocationId(),
                'dateFrom' => $equipmentProjection->getDateFrom()->format(DATE_ATOM),
                'dateUntil' => $equipmentProjection->getDateUntil()->format(DATE_ATOM),
            ])
            ->fetchAllAssociative();
    }
}
