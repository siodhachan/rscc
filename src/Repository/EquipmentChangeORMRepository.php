<?php

namespace App\Repository;

use App\Entity\EquipmentChange;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class EquipmentChangeORMRepository extends ServiceEntityRepository implements EquipmentChangeRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EquipmentChange::class);
    }

    public function add(EquipmentChange $change): void
    {
        $this->_em->persist($change);
        $this->_em->flush();
    }
}
