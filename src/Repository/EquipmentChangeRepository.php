<?php

namespace App\Repository;

use App\Entity\EquipmentChange;

interface EquipmentChangeRepository
{
    public function add(EquipmentChange $change): void;
}
