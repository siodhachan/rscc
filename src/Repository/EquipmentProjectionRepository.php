<?php

namespace App\Repository;

use App\Dto\EquipmentProjection\TimeLine;
use App\Dto\EquipmentProjectionQuery;

interface EquipmentProjectionRepository
{
    public function getTimeline(EquipmentProjectionQuery $equipmentProjection): TimeLine;
}
