<?php

namespace App\Tests\Application\Controller;

use App\DataFixtures\EquipmentChangeFixtures;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @coversDefaultClass \App\Controller\EquipmentStockProjectionController
 */
class EquipmentStockProjectionControllerTest extends WebTestCase
{
    /**
     * @test
     * @covers ::index
     *
     * @throws \JsonException
     */
    public function getWithExistingLocationAndDataReturnsTimeline(): void
    {
        //arrange
        $client = static::createClient();
        $databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
        $databaseTool->loadFixtures([EquipmentChangeFixtures::class]);

        //act
        $client->request(
            'GET',
            '/equipment/stock-projection/1',
            [
                'dateFrom' => '2020-01-01',
                'dateUntil' => '2020-01-09',
            ]
        );

        //assert
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('content-type'));
        $this->assertCount(10, json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR)['points']);
    }

    /**
     * @test
     * @covers ::index
     *
     * @throws \JsonException
     */
    public function getWithNonExistentLocationReturnsEmptyResponse(): void
    {
        //arrange
        $client = static::createClient();

        //act
        $client->request(
            'GET',
            '/equipment/stock-projection/9999999',
        );

        //assert
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertCount(0, json_decode($client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR)['points']);
    }

    /**
     * @test
     * @covers       ::index
     * @dataProvider invalidDatesProvider
     *
     * @throws \JsonException
     */
    public function getWithInvalidDatesReturnsError(
        string $dateFrom,
        string $dateUntil
    ): void {
        //arrange
        $client = static::createClient();

        //act
        $client->request(
            'GET',
            '/equipment/stock-projection/1',
            [
                'dateFrom' => $dateFrom,
                'dateUntil' => $dateUntil,
            ]
        );

        //assert
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function invalidDatesProvider(): \Generator
    {
        yield [
            '2020-01-01',
            '2019-01-01',
        ];

        yield [
            '2020-01-01',
            '2030-01-01',
        ];
    }
}
