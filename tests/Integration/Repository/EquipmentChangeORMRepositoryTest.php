<?php

namespace App\Tests\Integration\Repository;

use App\Entity\EquipmentChange;
use App\Entity\EquipmentChangeType;
use App\Repository\EquipmentChangeORMRepository as Subject;
use Doctrine\DBAL\Connection;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Webmozart\Assert\Assert;

class EquipmentChangeORMRepositoryTest extends KernelTestCase
{
    private Subject $subject;
    private AbstractDatabaseTool $databaseTool;

    public function setUp(): void
    {
        self::bootKernel();
        $container = static::getContainer();

        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();

        /** @var Subject $subject */
        $subject = $container->get(Subject::class);
        Assert::isInstanceOf($subject, Subject::class);
        $this->subject = $subject;
    }

    /**
     * @test
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function addAddsChange(): void
    {
        //arrange
        $this->databaseTool->withDatabaseCacheEnabled(false)->loadFixtures();
        $change = new EquipmentChange(
            1,
            2,
            new \DateTimeImmutable(),
            +5,
            EquipmentChangeType::Correction
        );

        //act
        $this->subject->add($change);

        //assert
        /** @var Connection $connection */
        $connection = static::getContainer()->get(Connection::class);
        $item = $connection
            ->prepare('SELECT * FROM equipment_change where id = :id')
            ->executeQuery(
                [
                    'id' => $change->getId(),
                ]
            )
            ->fetchOne();

        $this->assertNotFalse($item);
    }
}
