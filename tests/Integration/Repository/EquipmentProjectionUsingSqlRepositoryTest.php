<?php

namespace App\Tests\Integration\Repository;

use App\DataFixtures\EquipmentChangeFixtures;
use App\Dto\EquipmentProjectionQuery;
use App\Repository\EquipmentProjectionUsingSqlRepository as Subject;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Webmozart\Assert\Assert;

class EquipmentProjectionUsingSqlRepositoryTest extends KernelTestCase
{
    private Subject $subject;
    private AbstractDatabaseTool $databaseTool;

    public function setUp(): void
    {
        self::bootKernel();
        $container = static::getContainer();

        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();

        /** @var Subject $subject */
        $subject = $container->get(Subject::class);
        Assert::isInstanceOf($subject, Subject::class);
        $this->subject = $subject;
    }

    /**
     * @test
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function projectReturnsProjection(): void
    {
        //arrange
        $this->databaseTool->loadFixtures(
            [
                EquipmentChangeFixtures::class,
            ]
        );

        //act
        $result = $this->subject->getTimeline(
            new EquipmentProjectionQuery(
                1,
                new \DateTimeImmutable('2020-01-01'),
                new \DateTimeImmutable('2020-02-11')
            )
        );

        //assert
        //just do a sample test here; if aggregate values match at certain points the overall result must be fine
        $this->assertEquals(0, $result->points['2020-02-09']->equipments[1]->aggregate);
        $this->assertEquals(-2, $result->points['2020-02-10']->equipments[1]->aggregate);
    }
}
