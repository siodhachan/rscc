<?php

namespace App\Tests\Unit\EventSubscriber;

use App\Entity\EquipmentChange;
use App\Event\BookingWasCreated;
use App\EventSubscriber\AddChangeFromBookingCreation as Subject;
use App\Repository\EquipmentChangeRepository;
use PHPUnit\Framework\TestCase;

class AddChangeFromBookingCreationTest extends TestCase
{
    /**
     * @test
     */
    public function onBookingCreatedWithValidEventAddsChange()
    {
        $repository = $this->createMock(EquipmentChangeRepository::class);
        $repository->expects($this->exactly(4))
            ->method('add')
            ->with($this->isInstanceOf(EquipmentChange::class));

        $subject = new Subject($repository);
        $event = new BookingWasCreated(
            1,
            2,
            new \DateTimeImmutable(),
            new \DateTimeImmutable(),
            [1 => 1, 2 => 2]
        );

        $subject->onBookingWasCreated($event);
    }
}
