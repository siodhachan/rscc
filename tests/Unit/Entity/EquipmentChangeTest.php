<?php

namespace App\Tests\Unit\Entity;

use App\Entity\EquipmentChange;
use App\Entity\EquipmentChangeType;
use PHPUnit\Framework\TestCase;

class EquipmentChangeTest extends TestCase
{
    /**
     * @test
     * @dataProvider dataProviderEntityArguments
     */
    public function constructorConstructsOnlyValidVariants(
        $equipmentId,
        $locationId,
        $date,
        $value,
        $type,
        $expectedException
    ): void {
        if ($expectedException) {
            $this->expectException($expectedException);
        } else {
            $this->expectNotToPerformAssertions();
        }

        new EquipmentChange(
            $equipmentId,
            $locationId,
            $date,
            $value,
            $type
        );
    }

    public function dataProviderEntityArguments(): \Generator
    {
        yield [
            0,
            1,
            new \DateTimeImmutable(),
            1,
            EquipmentChangeType::Correction,
            \InvalidArgumentException::class,
        ];

        yield [
            1,
            0,
            new \DateTimeImmutable(),
            1,
            EquipmentChangeType::Correction,
            \InvalidArgumentException::class,
        ];

        yield [
            1,
            1,
            new \DateTimeImmutable(),
            1,
            EquipmentChangeType::Correction,
            null,
        ];
    }
}
