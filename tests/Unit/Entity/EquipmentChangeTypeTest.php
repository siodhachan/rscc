<?php

namespace App\Tests\Unit\Entity;

use App\Entity\EquipmentChangeType as Subject;
use PHPUnit\Framework\TestCase;

class EquipmentChangeTypeTest extends TestCase
{
    /**
     * @test
     * @dataProvider dataProviderValues
     */
    public function assertValuesAssertsIfValueMatchesType(
        $value,
        $type,
        $expectedException
    ): void {
        if ($expectedException) {
            $this->expectException($expectedException);
        } else {
            $this->expectNotToPerformAssertions();
        }

        Subject::assertValueIsValid($type, $value);
    }

    public function dataProviderValues(): \Generator
    {
        yield [
            1,
            Subject::Correction,
            null,
        ];

        yield [
            0,
            Subject::Correction,
            \InvalidArgumentException::class,
        ];

        yield [
            1,
            Subject::StockUp,
            null,
        ];

        yield [
            0,
            Subject::StockUp,
            \InvalidArgumentException::class,
        ];

        yield [
            -1,
            Subject::BookingOut,
            null,
        ];

        yield [
            0,
            Subject::BookingOut,
            \InvalidArgumentException::class,
        ];

        yield [
            1,
            Subject::BookingOut,
            \InvalidArgumentException::class,
        ];

        yield [
            1,
            Subject::BookingReturn,
            null,
        ];

        yield [
            0,
            Subject::BookingReturn,
            \InvalidArgumentException::class,
        ];

        yield [
            -1,
            Subject::BookingReturn,
            \InvalidArgumentException::class,
        ];
    }
}
